#pragma once

class Vector2
{
public:
	float x;
	float y;

	Vector2()
		: Vector2(0.0f, 0.0f)
	{ }

	Vector2(float x, float y)
		: x(x)
		, y(y)
	{ }
};
