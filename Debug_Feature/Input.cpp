#include "Input.h"

#include <SDL.h>

Input* Input::s_pInstance = nullptr;

Input::Input()
{

}

bool Input::GetKey(SDL_KeyCode code)
{
	return m_pressed[code];
}

bool Input::GetKeyUp(SDL_KeyCode code)
{
	if (m_pressed[code])
		return false;

	if (!m_up[code])
	{
		m_up[code] = true;
		return true;
	}

	return false;
}

bool Input::GetKeyDown(SDL_KeyCode code)
{
	if (!m_pressed[code])
		return false;

	if (!m_down[code])
	{
		m_down[code] = true;
		return true;
	}

	return false;
}

bool Input::ProcessEvents()
{
	SDL_Event evt;

	while (SDL_PollEvent(&evt) != 0)
	{
		char key = evt.key.keysym.sym;

		switch (evt.type)
		{
		case SDL_MOUSEBUTTONDOWN:
		{
			// TODO: ..
		}
		break;
		case SDL_MOUSEBUTTONUP:
		{
			// TODO: ..
		}
		break;
		case SDL_KEYUP:
		{
			m_down[key] = false;
			m_pressed[key] = false;
		}
		break;
		case SDL_KEYDOWN:
		{
			m_up[key] = false;
			m_pressed[key] = true;
		}
		break;

		case SDL_WINDOWEVENT:
		{
			if (evt.window.event == SDL_WINDOWEVENT_CLOSE)
				return false;
		}
		break;
		}
	}
	return true;
}
