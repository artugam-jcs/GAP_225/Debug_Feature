#pragma once

#include <unordered_map>
#include <SDL_keyboard.h>

class Input
{
private:
	static Input* s_pInstance;

	std::unordered_map<char, bool> m_down;
	std::unordered_map<char, bool> m_up;
	std::unordered_map<char, bool> m_pressed;

public:
	static Input* GetInstance() 
	{
		if (s_pInstance == nullptr)
			s_pInstance = new Input();
		return s_pInstance;
	}

	bool GetKey(SDL_KeyCode code);
	bool GetKeyUp(SDL_KeyCode code);
	bool GetKeyDown(SDL_KeyCode code);

	bool ProcessEvents();

private:
	Input();
};

