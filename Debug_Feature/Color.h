#pragma once

class Color
{
public:
	float r;
	float g;
	float b;

	Color()
		: Color(0, 0, 0)
	{ }

	Color(float r, float g, float b)
		: r(r)
		, g(g)
		, b(b)
	{ }
};

