#include "Player.h"

#include "Input.h"
#include "Color.h"

Player::Player()
{
	m_pRect = new SDL_Rect();

	m_pRect->w = 50;
	m_pRect->h = 50;

	m_color.r = 0;
	m_color.g = 255;
	m_color.b = 0;
}

Player::~Player()
{
	delete m_pRect;
}

void Player::Update(double deltaTime)
{
	auto input = Input::GetInstance();

#if _DEBUG
	std::cout << this->position.x << std::endl;

	/**
	 * @description Debug Feature
	 * 
	 * Usage case:
	 * 
	 * When you are debugging your game, sometime you want your player to move
	 * faster to move to the next level, etc. In that case of scenario, it's
	 * very useful! :P
	 */
	if (input->GetKey(SDLK_k))
		m_speed = 500.0f;   // Make it faster!
	if (input->GetKey(SDLK_l))
		m_speed = 200.0f;   // Revert!
#endif

	if (input->GetKey(SDLK_d))
		m_velocity.x = m_speed;
	else if (input->GetKey(SDLK_a))
		m_velocity.x = -m_speed;
	else
		m_velocity.x = 0;

	if (input->GetKey(SDLK_w))
		m_velocity.y = -m_speed;
	else if (input->GetKey(SDLK_s))
		m_velocity.y = m_speed;
	else
		m_velocity.y = 0;

	this->position.x += m_velocity.x * deltaTime;
	this->position.y += m_velocity.y * deltaTime;

	m_pRect->x = this->position.x;
	m_pRect->y = this->position.y;
}


void Player::Render(SDL_Renderer * pRenderer)
{
	SDL_SetRenderDrawColor(pRenderer, m_color.r, m_color.g, m_color.b, SDL_ALPHA_OPAQUE);

	SDL_RenderFillRect(pRenderer, m_pRect);
}