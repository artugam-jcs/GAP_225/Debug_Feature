# Debug_Feature

Please check the `Player.cpp` file's `Update` function.

```cpp
void Player::Update(double deltaTime)
{
	auto input = Input::GetInstance();

#if _DEBUG
	std::cout << this->position.x << std::endl;

	/**
	 * @description Debug Feature
	 * 
	 * Usage case:
	 * 
	 * When you are debugging your game, sometime you want your player to move
	 * faster to move to the next level, etc. In that case of scenario, it's
	 * very useful! :P
	 */
	if (input->GetKey(SDLK_k))
		m_speed = 500.0f;   // Make it faster!
	if (input->GetKey(SDLK_l))
		m_speed = 200.0f;   // Revert!
#endif

...
```
